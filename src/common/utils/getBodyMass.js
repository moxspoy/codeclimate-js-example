export default (height, mass) => {
    const b = height;
    return (height - mass) / 2;
}